// Bài 5 : In số nguyên tố

document.querySelector('.btn-danger').addEventListener('click', function() {
    var primeNumber = +document.querySelector('.prime-number').value;
    var result_5 = document.querySelector('.bg-danger');
    var contentHTML = '';
    function checkPrimes(t) {
        var check = true;
        if(t < 2) {
            check = false;
        } else if(t == 2) {
            check = true;
        } else if(t % 2 == 0) {
            check = false;
        } else {
            for(var c = 3; c <= Math.sqrt(t); c += 2) {
                if(t % c == 0) {
                    check = false;
                    break;
                }
            }
        }
        return check;
    }

    for(var b = 1; b <= primeNumber; b++) {
        if(checkPrimes(b)) {
            contentHTML += b + ' ';
        }
    }
    result_5.innerHTML = `Kết quả : ${contentHTML}`;
});